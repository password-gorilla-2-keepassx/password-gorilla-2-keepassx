#! /usr/bin/env python

import csv
import sys

csvfile = sys.argv[1]

csv.register_dialect('custom',
                     delimiter=';',
                     doublequote=True,
                     escapechar=None,
                     quotechar='"',
                     quoting=csv.QUOTE_MINIMAL,
                     skipinitialspace=False)

newdata = list()

with open(csvfile) as ifile:
    data = csv.reader(ifile, dialect='custom')
    for record in data:
        newdata.append(record)

for ii in range(0, len(newdata)):
    newdata[ii] = newdata[ii][1:]

for y in range(0, len(newdata)-1):
    for x in range(0, len(newdata[y])-1):
        newdata[y][x] = newdata[y][x].replace('&', '&amp;').replace('<', '&lt;').replace('\n', '<br/>')

newdata.sort()

# set variable for the previous group name and group level
isfirstgroup = True
oldname = None
oldlevel = 0
print "<!DOCTYPE KEEPASSX_DATABASE>\n<database>"
for ii in newdata:
  grouplevel = ii[0].count('.')
  groupname = ii[0][ii[0].rfind('.')+1:]
  if isfirstgroup:
    print '<group>'
    print "<title>{}</title>".format(groupname)
    oldname = groupname
    oldlevel = grouplevel
    isfirstgroup = False
  elif groupname != oldname and grouplevel == oldlevel:
    print '</group>'
    print '<group>'
    print "<title>{}</title>".format(groupname)
    oldname = groupname
  elif grouplevel > oldlevel:
    if grouplevel - oldlevel != 1:
      print '-----------------------------ERROR, please integrate groups---------------'
    print '<group>'
    print "<title>{}</title>".format(groupname)
    oldname = groupname
    oldlevel = grouplevel
  elif grouplevel < oldlevel:
    for xxx in range (0, oldlevel-grouplevel+1):
      print '</group>'
    print '<group>'
    print "<title>{}</title>".format(groupname)
    oldname = groupname
    oldlevel = grouplevel
  print '<entry>'
  print "<title>{}</title>".format(ii[1])
  print "<username>{}</username>".format(ii[2])
  print "<password>{}</password>".format(ii[3])
  print "<comment>{}</comment>".format(ii[4])
  print "</entry>"

print "</group>"
print "</database>"

# 0- ignore
# 1- group
# 2- title
# 3- user
# 4- pw
# 5- note
